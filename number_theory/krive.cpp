/* ** Criado em 08:05:41 ** */
/* ** Template v1.9 by GovBoy 23.01.2024 ** */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using ull = unsigned long long;

#ifdef EBUG
	#include "bug.hpp"
#else
	#define bug(x) { ; }
	#define bug2(x,y) { ; }
	#define bug3(x,y,z) { ; }
	#define bugV(V) { ; }
	#define bugP(V) { ; }
	#define bugL() { ; }
#endif

#define all(v) v.begin(), v.end()
#define esac(s) { cout << s << '\n'; return; }

// https://codeforces.com/group/yg7WhsFsAp/contest/355493/problem/P21
// http://g4m3.c073.com/problemas/P0069/
namespace krive {

	const int MAXK = 10'000'003; 
	bool is_comp_data[ MAXK+1 ];
	bool good = false;

	void proccess(){
		for( ll i=2; i*i<=MAXK; i++ )
			for( ll j=i*i; j<=MAXK; j+=i )
				is_comp_data[j] = true;
		good = true;
	}

	bool is_comp( int x ){
		if( not good ) proccess();
		return is_comp_data[x];
	} 

	bool is_prime( int x ){ return not is_comp(x); }
};

namespace K = krive;

void solve(){

	int N; cin >> N;
	vector<int> P; P.reserve( int(2*log2(N)) );
	for( int i=2; i<=N; i++ )
		if( K::is_prime(i) )
			P.push_back(i);

	cout << P.size() << '\n';
	for( auto x: P ) cout << x << ' '; cout << '\n';

}

signed main( void ){
	clock_t start_time = clock();
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	//int T; cin >> T; while( T-- )
	solve();

	double total_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	bugL(); bug(total_time);
	return 0;
}
