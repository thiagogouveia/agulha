/* ** Criado em 15:23:32 ** */
/* ** Template v1.9 by GovBoy 23.01.2024 ** */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using ull = unsigned long long;

#ifdef EBUG
	#include "bug.hpp"
#else
	#define bug(x) { ; }
	#define bug2(x,y) { ; }
	#define bug3(x,y,z) { ; }
	#define bugV(V) { ; }
	#define bugP(V) { ; }
	#define bugL() { ; }
#endif

#define all(v) v.begin(), v.end()
#define esac(s) { cout << s << '\n'; return; }

// ------------------------------------------------------
// Tarjan Bridge Detection ------------------------------
// Teste on: https://codeforces.com/contest/1986/problem/F
namespace Tarjan {
    using adj_list = vector<vector<int>> ;
    using set_of_edges = set<pair<int, int>> ;

    adj_list __place_holder;
    adj_list &V = __place_holder;
    set_of_edges bridges;
    vector<int> low, disc;
    int Time;

    void dfsBR(int u, int p) {
        low[u] = disc[u] = ++Time;
        for (int& v : V[u]) {
            if (v == p) continue; // we don't want to go back through the same path.
                                  // if we go back is because we found another way back

            if (!disc[v]) { // if V has not been discovered before
                dfsBR(v, u);  // recursive DFS call
                if (disc[u] < low[v]){ // condition to find a bridge
                    bridges.insert({u, v});
                    bridges.insert({v, u});
                }
                low[u] = min(low[u], low[v]); // low[v] might be an ancestor of u
            }
            
            else // if v was already discovered means that we found an ancestor
                low[u] = min(low[u], disc[v]); // finds the ancestor with the least discovery time
        }
    }

    set_of_edges & find_bridges( adj_list &V, int N ) {

        Tarjan::V = V;
        low = vector<int>(N);
        disc = vector<int>(N);
        bridges.clear();
        Time = 0;

        for (int u = 0; u < N; u++)
            if (!disc[u])
                dfsBR(u, u);
        
        return bridges;
    }
}
// ------------------------------------------------------
// ------------------------------------------------------


ll sol=0ll;

int dfs( vector<int> & status, int x ){
	status[x] = 1;

	for( auto v: Tarjan::V[x] ){
		if( status[v] != -1 ) continue;
		status[x] += dfs(status, v);
		if( Tarjan::bridges.count({x,v}) > 0 ){
			ll a = status[v];
			ll b = status.size()-status[v];

			a = a*(a-1ll)/2ll;
			b = b*(b-1ll)/2ll;

			sol = min(sol,a+b);
		}
	}
	return status[x];
}

void solve(){

	int N, M; cin >> N >> M;	
	vector<vector<int>> V(N, vector<int>() );

	for( int i=0, x, y; i<M; i++ ){
		cin >> x >> y;
		V[x-1].push_back(y-1);
		V[y-1].push_back(x-1);
	}

    Tarjan::find_bridges( V, N );
	bugP(bridges);

	vector<int> status(N,-1);

	sol = N*(N-1ll)/2ll;
	dfs( status, 0 );
	bugV(status);

	esac( sol );
}

signed main( void ){
	clock_t start_time = clock();
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int T; cin >> T; while( T-- )
	solve();

	double total_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	bugL(); bug(total_time);
	return 0;
}