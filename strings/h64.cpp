/* ** Criado em 13:15:14 ** */
/* ** Template v1.9 by GovBoy 23.01.2024 ** */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using ull = unsigned long long;

#ifdef EBUG
    #include "bug.hpp"
#else
    #define bug(x) { ; }
    #define bug2(x,y) { ; }
    #define bug3(x,y,z) { ; }
    #define bugV(V) { ; }
    #define bugP(V) { ; }
    #define bugL() { ; }
#endif

#define all(v) v.begin(), v.end()
#define esac(s) { cout << s << '\n'; return; }


// ------------------------------------------------------------------
// https://codeforces.com/problemset/problem/2010/C2
 
// 29, 31, 37, 41, 43, 47, 53, 59, 61, 67
// 1000000007  1000000009
// 251815849312376719 587276463492071987
// 890446884934054681 996944844379695157
template<int p=67, ll m=251815849312376719>
struct H64 {
    #define H64_MAX_SIZE ( 1 * 1'000'000 + 5)
 
    ll sz = 0ll;
    ll hash = 0ll;
 
    static inline ll P[ H64_MAX_SIZE ];
    static inline ll I[ H64_MAX_SIZE ];
    static ll mul( __int128_t a, __int128_t b ){ return (ll)( a*b%m ); }
    static ll sub( __int128_t a, __int128_t b ){ return (ll)( (m+a-b)%m ); }
    static ll inv( ll i ) { return i <= 1 ? i : (m - mul(m/i, inv(m % i))) % m; }

    H64() {
        if( P[1] == p ) return;
        P[0] = 1ll;
        P[1] = p;
        I[1] = inv(p);
        for( int i=2; i<H64_MAX_SIZE; i++ ){
             P[i] = mul(P[i-1], p);
             I[i] = mul(I[i-1], I[1]);
        }
    }
};
 
// hash + 'c'
template<int p, ll m>
H64<p,m> operator+( H64<p,m> h, char c ){
    h.sz++;
    h.hash = (h.hash + h.mul(c, h.P[h.sz])) % m;
    return h;
}
 
// 'c' + hash
template<int p, ll m>
H64<p,m> operator+( char c, H64<p,m> h ){
    h.sz++;
    h.hash = (h.mul(h.hash,p) + h.mul(c,p) % m);
    return h;
}
 
using H1 = H64<>; 
// ------------------------------------------------------------------
// https://codeforces.com/contest/1326/problem/D2
// Preffix Hashing

H1 pre[H64_MAX_SIZE], suf[H64_MAX_SIZE];
int N; // size of string

void build_preffix( string &s ){
    pre[0] = H1() + s[0];
    for( int i=1; i<N; i++ ) pre[i] = pre[i-1] + s[i];
}

// |[ 0 1 2 ] l . . . r | . . .
ll preffix_query( int l, int r ){
    if( l == 0 ) return pre[r].hash;
    return H1::mul( H1::sub(pre[r].hash, pre[l-1].hash), H1::I[l] );
}

void build_suffix( string &s ){
    suf[N-1] = H1() + s[N-1];
    for( int i=N-2; i>=0; i-- ) suf[i] = suf[i+1] + s[i];
}

// . . .|l . . . r |[ . . N-1 ]
ll suffix_query( int l, int r ){
    if( r == N-1 ) return suf[l].hash;
    return H1::mul( H1::sub(suf[l].hash, suf[r+1].hash), H1::I[N-r-1] );
}
// ------------------------------------------------------------------


void solve(){

    string s; cin >> s;
    N = (int) s.size();

    bug2(s, N);

    if( N==1 ) esac(s);

    int l=0, r=N-1;
    while( l < r and s[l] == s[r] ) l++, r--;

    bug2(l,r);

    build_preffix( s );
    build_suffix( s );
    
    //bug2( preffix_query(3,9), suffix_query(3,9) );
 
    static auto ispal = [](int _l, int _r){
        if( _l == _r ) return true;
        int delta = _r - _l + 1;
        int size = (delta / 2) - 1;
        return preffix_query(_l,_l+size) == suffix_query(_r-size, _r);
    };

    pair<int,int> bp = {-1,-1};
    for( int i=l; i<=r; i++ )
        if( ispal(l,i) ) bp = {l,i};

    pair<int,int> bs = {-1,-1};
    for( int i=r; i>=l; i-- )
        if( ispal(i,r) ) bs = {i,r};

    bug2( bp.first, bp.second );
    bug2( bs.first, bs.second );

    for( int i=0; i<l; i++ ) cout << s[i];

    if( (bp.second - bp.first) < (bs.second - bs.first)) swap( bp, bs);
    if( bp.first != -1)
        for( int i=bp.first; i<=bp.second; i++ ) cout << s[i];

    for( int i=r+1; i<N; i++ ) cout << s[i];

    cout << '\n';
}

signed main( void ){
    clock_t start_time = clock();
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

    int T; cin >> T; while( T-- )
    solve();

    double total_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
    bugL(); bug(total_time);
    return 0;
}
